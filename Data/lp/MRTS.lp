/* Please email questions, comments, and significant results to: Robert.C.Green@gmail.com.  Thanks!

 (c) 2009-2010 Zhu Wang, Robert Green

 ALL RIGHTS RESERVED WORLDWIDE

 THIS PROGRAM IS FREEWARE.  IT MAY BE COPIED AND
 DISTRIBUTED WITHOUT LIMITATION AS LONG AS THIS
 COPYRIGHT NOTICE AND REFERENCE
 INFORMATION BELOW ARE INCLUDED WITHOUT MODIFICATION,
 AND AS LONG AS NO FEE OR COMPENSATION IS CHARGED,
 INCLUDING "TIE-IN" OR "BUNDLING" FEES CHARGED FOR
 OTHER PRODUCTS.

 This code implements the MRTS model DC Optimal Power FLow using LP_Solve

 References concerning this code include:
 1 - R. Green, L. Wang, Z. Wang, M. Alam, and C. Singh, “Power System Reliability Assessment Using Intelligent State Space Pruning Techniques: A Comparative Study” Submitted to 2010 Conference on Power System Technology, Hangzou China, October 2010.
 2 - R. Green, L. Wang, M. Alam, and C. Singh, “State space pruning for Reliability Evaluation using Binary Particle Swarm Optimization,” Submitted to Hawaii International Conference on System Sciences,University of Hawaii at Manoa, January 2011.
 3 - R. Green, L. Wang, and C. Singh, “State space pruning for power system reliability evaluation using genetic algorithms,” IEEE Power & Energy Society General Meeting 2010, Minneapolis, MN, July 2010.
 4 - R. Green, Z. Wang, L. Wang, M. Alam, and C. Singh, “Evaluation of loss of load probability for power systems using intelligent search based state space pruning,” The 11th International Conference on Probabilistic Methods Applied to Power Systems, Singapore, June 2010*/

/* Objective function */
min: C1 +C2 +C3 +C4 +C5 +C6 +C7 +C8 +C9 +C10 +C11 +C12 +C13 +C14 +C15 +C16 +C18 +C19 +C20 +C21 +C22 +C23 +C24;

LOAD_1:+88.5116T1 -71.9424T2 -4.7348T3 -11.8343T5								-1G1 -C1 	=-1.9440;
LOAD_2:-71.9424T1 +85.0434T2 -7.8927T4 -5.2083T6								-1G2 -C2 	=-1.7460;
LOAD_3:-4.7348T1 +24.8810T3 -8.4034T9 -11.7428T24								-1G3 -C3 	=-3.2400;
LOAD_4:-7.8927T2 +17.5359T4 -9.6432T9											-1G4 -C4 	=-1.3320;
LOAD_5:-11.8343T1 +23.1593T5 -11.3250T10										-1G5 -C5 	=-1.2780;
LOAD_6:-5.2083T2 +21.7373T6 -16.5289T10										    -1G6 -C6 	=-2.4480;
LOAD_7:+16.2866T7 -16.2866T8													-1G7 -C7 	=-2.2500;
LOAD_8:-16.2866T7 +28.4005T8 -6.0569T9 -6.0569T10								-1G8 -C8 	=-3.0780;
LOAD_9:-8.4034T3 -9.6432T4 -6.0569T8 +47.2471T9 -11.5718T11 -11.5718T12		    -1G9 -C9 	=-3.1500;
LOAD_10:-11.3250T5 -16.5289T6 -6.0569T8 +57.3965T10 -11.7428T11 -11.7428T12		-1G10 -C10 	=-3.5100;
LOAD_11:-11.5718T9 -11.7428T10 +68.2465T11 -21.0084T13 -23.9234T14				-1G11 -C11 	=-0.0000;
LOAD_12:-11.5718T9 -11.7428T10 +54.6750T12 -21.0084T13 -10.3520T23				-1G12 -C12 	=-0.0000;
LOAD_13:-21.0084T11 -21.0084T12 +53.5775T13 -11.5607T23							-1G13 -C13 	=-4.7700;
LOAD_14:-23.9234T11 +49.6304T14 -25.7069T16										-1G14 -C14 	=-3.4920;
LOAD_15:+117.8876T15 -57.8035T16 -40.8163T21 -19.2678T24						-1G15 -C15 	=-5.7060;
LOAD_16:-25.7069T14 -57.8035T15 +165.4105T16 -38.6100T17 -43.2900T19			-1G16 -C16 	=-1.8000;
LOAD_17:-38.6100T16 +117.5512T17 -69.4444T18 -9.4967T22							-1G17 -C17 	=-0.0000;
LOAD_18:-69.4444T17 +146.6645T18 -77.2201T21									-1G18 -C18 	=-5.9940;
LOAD_19:-43.2900T16 +93.7951T19 -50.5051T20										-1G19 -C19 	=-3.2580;
LOAD_20:-50.5051T19 +143.0976T20 -92.5926T23									-1G20 -C20 	=-2.3040;
LOAD_21:-40.8163T15 -77.2201T18 +132.7857T21 -14.7493T22						-1G21 -C21 	=-0.0000;
LOAD_22:-9.4967T17 -14.7493T21 +24.2459T22										-1G22 -C22 	=-0.0000;
LOAD_23:-10.3520T12 -11.5607T13 -92.5926T20 +114.5053T23						-1G23 -C23 	=-0.0000;
LOAD_24:-11.7428T3 -19.2678T15 +31.0106T24										-1G24 -C24 	=-0.0000;
 
+1G1 	<= 3.84;
+1G2 	<= 3.84;
+1G3 	<= 0;
+1G4 	<= 0;
+1G5 	<= 0;
+1G6 	<= 0;
+1G7 	<= 6.00;
+1G8 	<= 0;
+1G9 	<= 0;
+1G10 	<= 0;
+1G11 	<= 0;
+1G12 	<= 0;
+1G13 	<= 11.82;
+1G14 	<= 0;
+1G15 	<= 4.30;
+1G16 	<= 3.10;
+1G17	<= 0;
+1G18 	<= 8.00;
+1G19 	<= 0;
+1G20 	<= 0;
+1G21 	<= 8.00;
+1G22 	<= 6.00;
+1G23 	<= 13.20;
+1G24 	<= 0;

CURTAIL_1: +1C1 	<= 1.9440;
CURTAIL_2: +1C2 	<= 1.746;
CURTAIL_3: +1C3 	<= 3.24;
CURTAIL_4: +1C4 	<= 1.332;
CURTAIL_5: +1C5 	<= 1.278;
CURTAIL_6: +1C6 	<= 2.448;
CURTAIL_7: +1C7 	<= 2.25;
CURTAIL_8: +1C8 	<= 3.078;
CURTAIL_9: +1C9 	<= 3.15;
CURTAIL_10: +1C10 	<= 3.51;
CURTAIL_11: +1C11 	<= 0;
CURTAIL_12: +1C12 	<= 0;
CURTAIL_13: +1C13 	<= 4.77;
CURTAIL_14: +1C14	<= 3.492;
CURTAIL_15: +1C15 	<= 5.706;
CURTAIL_16: +1C16 	<= 1.8;
CURTAIL_17: +1C17 	<= 0;
CURTAIL_18: +1C18 	<= 5.994;
CURTAIL_19: +1C19 	<= 3.258;
CURTAIL_20: +1C20 	<= 2.304;
CURTAIL_21: +1C21 	<= 0;
CURTAIL_22: +1C22 	<= 0;
CURTAIL_23: +1C23 	<= 0;
CURTAIL_24: +1C24 	<= 0;

F1_1: +71.942T1  -71.942T2     <= 1.93;
F1_2: +4.7348T1  -4.7348T3     <= 2.08;
F1_3: +11.834T1  -11.834T5     <= 2.08;
F1_4: +7.8927T2  -7.8927T4     <= 2.08;
F1_5: +5.2083T2  -5.2083T6     <= 2.08;
F1_6: +8.4034T3  -8.4034T9     <= 2.08;
F1_7: +11.743T3  -11.743T24    <= 5.10;
F1_8: +9.6432T4  -9.6432T9     <= 2.08;
F1_9: +11.325T5  -11.325T10    <= 2.08;
F1_10: +16.529T6  -16.529T10   <= 1.93;
F1_11: +16.287T7  -16.287T8    <= 2.08;
F1_12: +6.0569T8  -6.0569T9    <= 2.08;
F1_13: +6.0569T8  -6.0569T10   <= 2.08;
F1_14: +11.572T9  -11.572T11   <= 5.10;
F1_15: +11.572T9  -11.572T12   <= 5.10;
F1_16: +11.743T10 -11.743T11   <= 5.10;
F1_17: +11.743T10 -11.743T12   <= 5.10;
F1_18: +21.008T11 -21.008T13   <= 6.00;
F1_19: +23.923T11 -23.923T14   <= 6.00;
F1_20: +21.008T12 -21.008T13   <= 6.00;
F1_21: +10.352T12 -10.352T23   <= 6.00;
F1_22: +11.561T13 -11.561T23   <= 6.00;
F1_23: +25.707T14 -25.707T16   <= 6.00;
F1_24: +57.803T15 -57.803T16   <= 6.00;
F1_25: +20.408T15 -20.408T21   <= 6.00;
F1_26: +20.408T15 -20.408T21   <= 6.00;
F1_27: +19.268T15 -19.268T24   <= 6.00;
F1_28: +38.61T16  -38.61T17    <= 6.00;
F1_29: +43.29T16  -43.29T19    <= 6.00;
F1_30: +69.444T17 -69.444T18   <= 6.00;
F1_31: +9.4967T17 -9.4967T22   <= 6.00;
F1_32: +38.61T18  -38.61T21    <= 6.00;
F1_33: +38.61T18  -38.61T21    <= 6.00;
F1_34: +25.253T19 -25.253T20   <= 6.00;
F1_35: +25.253T19 -25.253T20   <= 6.00;
F1_36: +46.296T20 -46.296T23   <= 6.00;
F1_37: +46.296T20 -46.296T23   <= 6.00;
F1_38: +14.749T21 -14.749T22   <= 6.00;

F2_1: -71.942T1  +71.942T2      <= 1.93;
F2_2: -4.7348T1  +4.7348T3      <= 2.08;
F2_3: -11.834T1  +11.834T5      <= 2.08;
F2_4: -7.8927T2  +7.8927T4      <= 2.08;
F2_5: -5.2083T2  +5.2083T6      <= 2.08;
F2_6: -8.4034T3  +8.4034T9      <= 2.08;
F2_7: -11.743T3  +11.743T24     <= 5.10;
F2_8: -9.6432T4  +9.6432T9      <= 2.08;
F2_9: -11.325T5  +11.325T10     <= 2.08;
F2_10: -16.529T6  +16.529T10 	<= 1.93;
F2_11: -16.287T7  +16.287T8  	<= 2.08;
F2_12: -6.0569T8  +6.0569T9  	<= 2.08;
F2_13: -6.0569T8  +6.0569T10 	<= 2.08;
F2_14: -11.572T9  +11.572T11 	<= 5.10;
F2_15: -11.572T9  +11.572T12 	<= 5.10;
F2_16: -11.743T10 +11.743T11 	<= 5.10;
F2_17: -11.743T10 +11.743T12 	<= 5.10;
F2_18: -21.008T11 +21.008T13 	<= 6.00;
F2_19: -23.923T11 +23.923T14 	<= 6.00;
F2_20: -21.008T12 +21.008T13 	<= 6.00;
F2_21: -10.352T12 +10.352T23 	<= 6.00;
F2_22: -11.561T13 +11.561T23 	<= 6.00;
F2_23: -25.707T14 +25.707T16 	<= 6.00;
F2_24: -57.803T15 +57.803T16 	<= 6.00;
F2_25: -20.408T15 +20.408T21 	<= 6.00;
F2_26: -20.408T15 +20.408T21 	<= 6.00;
F2_27: -19.268T15 +19.268T24 	<= 6.00;
F2_28: -38.61T16  +38.61T17  	<= 6.00;
F2_29: -43.29T16  +43.29T19  	<= 6.00;
F2_30: -69.444T17 +69.444T18 	<= 6.00;
F2_31: -9.4967T17 +9.4967T22 	<= 6.00;
F2_32: -38.61T18  +38.61T21  	<= 6.00;
F2_33: -38.61T18  +38.61T21  	<= 6.00;
F2_34: -25.253T19 +25.253T20 	<= 6.00;
F2_35: -25.253T19 +25.253T20 	<= 6.00;
F2_36: -46.296T20 +46.296T23 	<= 6.00;
F2_37: -46.296T20 +46.296T23 	<= 6.00;
F2_38: -14.749T21 +14.749T22 	<= 6.00;

/* Variable bounds */
G1  >= 0;
G2  >= 0;
G3  >= 0;
G4  >= 0;
G5  >= 0;
G6  >= 0;
G7  >= 0;
G8  >= 0;
G9  >= 0;
G10 >= 0;
G11 >= 0;
G12 >= 0;
G13 >= 0;
G14 >= 0;
G15 >= 0;
G16 >= 0;
G17 >= 0;
G18 >= 0;
G19 >= 0;
G20 >= 0;
G21 >= 0;
G22 >= 0;
G23 >= 0;
G24 >= 0;

C1  >= 0;
C2  >= 0;
C3  >= 0;
C4  >= 0;
C5  >= 0;
C6  >= 0;
C7  >= 0;
C8  >= 0;
C9  >= 0;
C10 >= 0;
C11 >= 0;
C12 >= 0;
C13 >= 0;
C14 >= 0;
C15 >= 0;
C16 >= 0;
C17 >= 0;
C18 >= 0;
C19 >= 0;
C20 >= 0;
C21 >= 0;
C22 >= 0;
C23 >= 0;
C24 >= 0;
